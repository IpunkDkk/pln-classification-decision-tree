import streamlit as st
import pandas as pd
import numpy as np
import pickle
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
import matplotlib.pyplot as plt

st.write("""
# ARUS NETRAL DIBANDING ARUS PHASE TERENDAH

This app predicts the **ARUS NETRAL DIBANDING ARUS PHASE TERENDAH**

Data Didapat Dari [PLN MADURA](https://www.instagram.com/pln_up3pamekasan/?hl=en).
""")



def user_input_features():
        var_r = st.number_input("R")
        var_s = st.number_input("S")
        var_t = st.number_input("T")
        var_n = st.number_input("N")
        var_r_n = st.number_input("R-N")
        var_s_n = st.number_input("S-N")
        var_t_n = st.number_input("T-N")
        delta = st.number_input("DELTA ARUS PHASA RMS MAX - MIN")

        data = {
              "R" : var_r,
              "S" : var_s,
              "T" : var_t,
              "N" : var_n,
              "R-N" : var_r_n,
              "S-N" : var_s_n,
              "T-N" : var_t_n,
              "DELTA ARUS PHASA RMS MAX - MIN" : delta,
              
        }
        
        features = pd.DataFrame(data, index=[0])
        return features


df = user_input_features()
st.subheader('Input Feture Bisa berupa')
st.write(df)
load_clf = pickle.load(open('classifier_listrik.pkl', 'rb'))
prediction = load_clf.predict(df)
prediction_proba = load_clf.predict_proba(df)
st.subheader('Classification')
st.write(prediction)

st.subheader('Prediction Probability')
st.write(prediction_proba)

st.subheader("Tree")
def plot_decision_tree(clf):
    # Set the size of the figure
    plt.figure(figsize=(10, 6))
    # Plot the decision tree
    tree.plot_tree(clf, filled=True, rounded=True)
    st.set_option('deprecation.showPyplotGlobalUse', False)
    # Show the plot
    st.pyplot()
plot_decision_tree(load_clf)


st.subheader("Data")
pln = pd.read_csv("DataListrik.csv")
st.write(pln.describe())

